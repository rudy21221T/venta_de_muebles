<?php 

namespace App\Models;
use CodeIgniter\Model;

class Producto extends Model
{
public function getProducto()
	{

		protected $table = 'producto';
	    protected $primaryKey = 'id_producto';

		$pa_consultar = 'Call pa_consultar()';
		$query = $this->query($pa_consultar);
		return $query->getResult();
	}