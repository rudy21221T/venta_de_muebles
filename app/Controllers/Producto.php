<?php 

namespace App\Controllers;
use CodeIgniter\Controller;
use App\Entities\User;
use App\Models\Producto;

class Producto extends Controller
{
	public function index()
	{
		$producto = new Producto();

		$data['producto'] = $producto->getProducto();
		$data['proveedor'] = $producto->getProveedor();
		echo view('templates/header', $data);
		echo view('producto/overview');
		echo view('templates/footer');
	   }

	//-------------------------------------------------------------------
	//--------------------------------------------------------------------	
}